tekore>=1.1.*
apscheduler==3.6.*
httpx==0.11.*
starlette>=0.12.11
itsdangerous
uvicorn